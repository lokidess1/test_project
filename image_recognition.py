from imageai.Detection import ObjectDetection, VideoObjectDetection
import cv2
# detector = ObjectDetection()
# # detector.setModelTypeAsRetinaNet()
# # detector.setModelPath('ai/resnet.h5')
# detector.setModelTypeAsYOLOv3()
# detector.setModelPath('ai/yolo.h5')
# detector.loadModel()
#
# if __name__ == '__main__':
#     detector.detectObjectsFromImage(
#         input_image='1.jpg', output_image_path='1_new.jpg',
#         # minimum_percentage_probability=95
#     )


camera = cv2.VideoCapture(0)

if __name__ == '__main__':
    # rval, frame = camera.read()
    # cv2.imwrite('screenshot.jpg', frame)
    detector = VideoObjectDetection()
    detector.setModelTypeAsYOLOv3()
    detector.setModelPath('ai/yolo.h5')
    detector.loadModel()

    detector.detectObjectsFromVideo(
        camera_input=camera, output_file_path="python_wfh_lesson", log_progress=True,
        frames_per_second=30
    )

    # if camera.isOpened():
    #     rval, frame = camera.read()
    # else:
    #     rval = False
    #
    # counter = 0
    # while rval:
    #     cv2.imshow("preview", frame)
    #     rval, frame = camera.read()
    #     key = cv2.waitKey(20)
    #     if key == 27:  # exit on ESC
    #         break
