from functools import reduce
lst = [1, 2, 3, 4, 5, 6]

# 1
# for key, item in enumerate(lst):
#     lst[key] = item + 1
#
# print(lst)

# 2
# lst = [x + 1 for x in lst]
# print(lst)

# 3


# def map_inner(x):
#     return x + 1
#
#
# lst = map(map_inner, lst)
# print(list(lst))

# def filter_inner(x):
#     return x % 2 == 0
#
#
# lst = filter(filter_inner, lst)
# print(list(lst))

# def reduce_inner(x, y):
#     print(x, y)
#     return x + y
#
#
# result = reduce(reduce_inner, lst)
# print(result)


class MySuperClass:
    pass


class Cat:
    name = "Zuma"
    age = 1
    tail_length = 3

    def __init__(self, name="Zuma", age=1, tail_length=3):
        self.name = name
        self.age = age
        self.tail_length = tail_length

    def __str__(self):
        return f"Name: {self.name}\nAge: {self.age}\nTail: {self.tail_length}"

    def __del__(self):
        print(f"Cat {self.name} died :'-(")

    # def __getattribute__(self, item):
    #     print(item)
    #     return item

    def serialize(self):
        return {
            'name': self.name,
            'age': self.age,
            'tail': self.tail_length
        }

    def say_meow(self):
        print(f"Cat {self.name} said meow!")


class Lion(Cat):

    def __init__(self, *args, **kwargs):
        self.speed = kwargs.pop('speed', 0.5)
        super(Lion, self).__init__(*args, **kwargs)

    def __del__(self):
        print(f"Lion {self.name} died :'-(")

    def say_meow(self):
        print(f"Lion {self.name} said meow!")

    def serialize(self, info=''):
        data = super(Lion, self).serialize()
        data['speed'] = self.speed
        if info:
            data['info'] = info
        return data


class Bird:
    name = 'Popka'
    age = 10

    def flight(self):
        print(f"Bird {self.name} flight now")

    def say_meow(self):
        print(f"Bird {self.name} said meow!")


class SwimMixin:

    def swim(self):
        print(f"{self.name} swim now")


class CatBird(Cat, Bird, SwimMixin):
    _protected = ''
    __private = ''

    def _my_protected_method(self):
        print('Protected')
        self.__my_private_method()

    def __my_private_method(self):
        print('Private')




# class SwimingLion(Lion, SwimMixin):
#     pass


# cat_bird = CatBird(name='Fluffy', age=12, tail_length=22)
#
# cat_bird.flight()
# cat_bird.say_meow()
# cat_bird.swim()
# cat_bird._my_protected_method()
# cat_bird._CatBird__my_private_method()


# lion_killer = Lion(name='Simba', age=10, tail_length=5, speed=1)
#
# lion_killer.say_meow()


# zuma_cat = Cat()
# tor_cat = Cat("Tor", 3, 5)
# magnus_cat = Cat(name="Magnus", tail_length=4, age=4)
#
#
# print(zuma_cat)
# print(tor_cat)
# print(magnus_cat)

# tor_cat.name = 'Tor'
# tor_cat.age = 3
# tor_cat.tail_length = 5

# zuma_cat.say_meow()
# tor_cat.say_meow()

result = zip([1, 2, 3, 4], [2, 3, 5], [1, 2])
print(list(result))


def sum(x, y):
    return x + y


print(sum(1, 2))
print(sum("1", "2"))


# C++

# int sum(int x, int y) {
#     return x + y;
# }
# str sum(str x, str y) {
#     return x + y;
# }
#
# sum(1, 1)
# sum("1", "2")

