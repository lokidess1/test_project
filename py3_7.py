import pickle
import json


some_list_of_data = ['1,1', 1, 2, [1, 2], 'qwe']
another = {'name': 'Loki'}
#
# with open('test.dump', 'wb') as dump_file:
#     pickle.dump(some_list_of_data, dump_file)
#
# with open('test.dump', 'ab') as dump_file:
#     pickle.dump(another, dump_file)
#
# with open('test.dump', 'rb') as dump_file:
#     some_list_of_data = pickle.load(dump_file)
#     another_data = pickle.load(dump_file)
#
# print(another_data, type(another_data))


# data = {
#   "accountant": None,
#   "car_carrier": None,
#   "company": 78,
#   "consignee": None,
#   "consignor": None,
#   "couchId": None,
#   "customer": None,
#   "date_time": "2020-03-15T16:56:06+02:00",
#   "driver": None,
#   "goods": [],
#   "id": 857,
#   "loading_point": "",
#   "measures_data": {
#     "first": {
#       "dateTime": "2020-03-15T16:56:06+02:00",
#       "images": [],
#       "weightValue": 26
#     },
#     "second": None
#   },
#   "publish_responsible": None,
#   "resync": True,
#   "scale_id": 35,
#   "storage_from": None,
#   "storage_to": None,
#   "sync_at": "2020-03-15T14:56:07.099153+0000",
#   "trailer": None,
#   "type": "weighting_event",
#   "unloading_point": "",
#   "user_id": 100,
#   "vehicle": None,
#   "weight_by_docs": None,
#   "weighting_mode": "auto",
#   "weighting_status": {
#     "confirmed": False,
#     "incomplete": True,
#     "pending": False
#   },
#   "weighting_type": "movement"
# }


# json_data = json.dumps(some_list_of_data, indent=2)
# print(json_data, type(json_data))
#
# with open('test.json', 'w') as json_file:
#     json.dump(data, json_file, indent=2)
#
#
# with open('test.json', 'r') as json_file:
#     some_new = json.load(json_file)
#     print(some_new['weighting_type'])


# import time
# import os
#
#
# def cache_it(file_name=None):
#     def inner_wrapper(func):
#         def wrapper(*args, **kwargs):
#             if file_name is not None:
#                 _file_name = file_name
#             else:
#                 _file_name = '_'.join([str(x) for x in args])
#                 _file_name = f'{file_name}.dump'
#
#             if os.path.exists(f'cache/{file_name}'):
#                 with open(f'cache/{file_name}', 'rb') as dump_file:
#                     return pickle.load(dump_file)
#
#             value = func(*args, **kwargs)
#
#             with open(f'cache/{file_name}', 'wb') as dump_file:
#                 pickle.dump(value, dump_file)
#
#             return value
#         return wrapper
#     return inner_wrapper
#
#
# @cache_it(file_name='test.dump')
# def some_long_function(first, second):
#     time.sleep(5)
#     return first + second
#
#
# print(some_long_function(11, 15))

# some_long_function(1, 2)

# def some_long_function1(first, second):
#     time.sleep(5)
#     return random.choice([first, second])

# some_long_function1(1, 2)
import multiprocessing
import threading
import time


def count_down(count):
    counter = 1
    while counter <= count:
        print(counter)
        counter += 1
        time.sleep(1)


def count_up(count):
    counter = count
    while counter > 0:
        print(counter)
        counter -= 1
        time.sleep(1)


# thread1 = threading.Thread(target=count_down, args=(10, ))
# thread2 = threading.Thread(target=count_up, args=(10, ))
#
# thread1.start()
# thread2.start()
# for x in range(1, 20):
#     print('----')
#     time.sleep(1)


def count(x):

    while x > 0:
        x -= 1


# count(100000000)
# t1 = threading.Thread(target=count, args=(100000000, ))
# t2 = threading.Thread(target=count, args=(100000000, ))

t1 = multiprocessing.Process(target=count, args=(100000000, ))
t2 = multiprocessing.Process(target=count, args=(100000000, ))

t1.start()
t2.start()

t1.join()
t2.join()
print('123')


# t = (1)

# NO PARAMS!
def decorator_name(decorated_function):

    # ACCEPT DECORATES FUNCTION VARS
    def wrapper_name(*args, **kwargs):
        # CODE BEFORE DECORATED FUNCTION
        value = decorated_function(*args, **kwargs)
        # CODE AFTER DECORATED FUNCTION
        return value
    return wrapper_name


# some_decorated_func()


# YES PARAMS!
def decorator_name(decorator_args):

    def inner_wrapper_name(decorated_function):
        # ACCEPT DECORATES FUNCTION VARS
        def wrapper_name(*args, **kwargs):
            # CODE BEFORE DECORATED FUNCTION
            value = decorated_function(*args, **kwargs)
            # CODE AFTER DECORATED FUNCTION
            return value
        return wrapper_name
    return inner_wrapper_name
