import os
from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.popup import Popup


class FileLoadLayout(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class FileSaveLayout(FloatLayout):
    save = ObjectProperty(None)
    cancel = ObjectProperty(None)
    filename = ObjectProperty(None)


class MainLayout(FloatLayout):
    popup = None

    @staticmethod
    def action_exit():
        exit(0)

    def action_close_popup(self):
        self.popup.dismiss()

    def action_load_file(self, path, filename):
        with open(os.path.join(path, filename[0])) as stream:
            self.ids.main_text.text = stream.read()
        self.action_close_popup()

    def action_save_file(self, path, filename):
        with open(os.path.join(path, filename), 'w') as stream:
            stream.write(self.ids.main_text.text)
        self.action_close_popup()

    def action_load(self):
        content = FileLoadLayout(load=self.action_load_file, cancel=self.action_close_popup)
        self.popup = Popup(title="Load file", size_hint=(0.8, 0.8), content=content)
        self.popup.open()

    def action_save(self):
        content = FileSaveLayout(save=self.action_save_file, cancel=self.action_close_popup)
        self.popup = Popup(title="Save file", size_hint=(0.8, 0.8), content=content)
        self.popup.open()


class Editor(App):
    pass


if __name__ == '__main__':
    Editor().run()
