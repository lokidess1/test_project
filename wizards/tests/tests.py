from unittest import TestCase, skip
from unittest.mock import patch, Mock

from wizards.main import Character


class CharacterTestCase(TestCase):

    # def setUp(self):
    #     self.character = Character()
    #     self.character.skills = {'test_skill', }

    # def tearDown(self):
    #     pass

    @classmethod
    def setUpClass(cls):
        cls.character = Character()
        cls.character.skills = {'test_skill', }

    @classmethod
    def tearDownClass(cls):
        pass

    # @patch('wizards.utils.skils.fireball')  # FAIL!
    # @patch('wizards.main.fireball')  # SUCCESS!
    @patch('wizards.main.skils')
    def test_use_random_skill(self, mock_skils):
        expected_value = 'test_skill_runned'
        mock_skils.test_skill = Mock(return_value='test_skill_runned')

        value = self.character.use_random_skill()

        self.assertEqual(expected_value, value)

    @patch('wizards.main.random.choice', return_value='test')
    @patch('wizards.main.skils')
    def test_use_random_skill_random(self, mock_skils, mock_choice):
        self.character.use_random_skill()

        mock_choice.assert_called_once_with(list(self.character.skills))
        # self.assertEqual(mock_choice.call_count, 2)
