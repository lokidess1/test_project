import random
from .utils import skils


class Character:

    skills = set()

    def use_random_skill(self):
        return getattr(skils, random.choice(list(self.skills)))()


class Game:

    available_skills = []
    character = None

    def __init__(self):
        self.available_skills = [x for x in dir(skils) if not x.startswith('__')]

    def main(self):
        self.character = Character()
        while True:
            value = input('Choice your move: ')
            if value == 'next':
                interaction_result = self.next_interaction()
                if interaction_result:
                    self.character.skills.add(interaction_result)
            elif value == 'spell':
                self.character.use_random_skill()
            elif value == 'show':
                print(self.character.skills)
            else:
                print('You move like a full')

    def next_interaction(self):
        if random.choice(range(3)) == 1:
            print('You got a new spell!')
            return self.obtain_random_skill()
        else:
            print('Nothing here')
        return None

    def obtain_random_skill(self):
        return random.choice(self.available_skills)


if __name__ == '__main__':
    game = Game()
    game.main()
