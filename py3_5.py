from pprint import pprint
#
#
# def my_function():
#     print("Hello world")
#
#
# third = 3
# new_value = 12
#
#
# def sum_me(first, second):
#     return first + second + third + new_value
#
#
# print(sum_me(4, 10))
# lst = [
#     {'name': "Loki", "age": 30},
#     {'name': "Odin", "age": 111},
#     {'name': "Tur", "age": 987},
#     {"name": "Tor", "age": 12}
# ]
#
#
# def key_func(x):
#     return x['age']
#
#
# lst = sorted(lst, key=key_func)
#
# pprint(lst)
#
#
# def some_math(a, b, c, callback):
#     result = a + b - c
#     callback()
#     return result
#
#
# def my_callback():
#     print('Math done!')
#
#
# print(some_math(1, 2, 3, my_callback))
#
#
#
#
# # my_function.my_param = 123
# # print(dir(my_function))


# def devide(first, second):
#     return first - second


# print(devide(10, 3))
# print(devide(second=3, first=10))
# print(devide(10, second=3))
# print(devide(3, first=10))


def some_math(a, b, c=10):
    if c:
        return a + b - c
    return a + b

kw = {
    'a': 1,
    'b': 2,
    'c': 3
}
lst = [1, 2, 3]
some_math(lst[0], lst[1], lst[2])
some_math(*lst)
# print(some_math(**kw))

# print(some_math(1, 2))
# print(some_math(1, 2, 3))

# def some_func():
#     a = 10
#
# print(some_func())


def infinite(*args):
    print(args)
    for x in args:
        print(x)


# lst = [1, 2, 3, 45]
# infinite(*lst)


# infinite(1, 2, 5, 5, 3, 2)

def infinite_names(**kwargs):
    pprint(kwargs)


# infinite_names(name='Loki', age=30, qwe='qwe')

def total_infinity(*args, **kwargs):
    print(args)
    print(kwargs)


# def sort_me(*args):
#     return sorted(args)


# total_infinity(1, 2, 3, 4, 'w', name='Loki', age=30)


def sum_dev(first, second):
    sum = first + second
    dev = first - second
    return sum, dev


# a, b = sum_dev(2, 5)
#
# a, b = 10, 12

# a, *b, c = [1, 2, 3, 4, 5, 6, 7]
# print(a)
# print(b)
# print(c)

# def some(a=[]):
#     a.append(1)
#     print(a)
#
#
# some()
# some()
# some()

# def endless(iter_count=10):
#     counter = 1
#     while counter < iter_count:
#         yield counter
#         counter += 1
#         yield counter + 2


# lst = list(endless(100))
# print(lst)
# en = endless(2)
# print(next(en))
# print(next(en))
# print(next(en))

# en2 = endless()
# for item in endless():
#     print(item)


# import random
#
#
# def a():
#     pass
#
#
# def b():
#     pass
#
#
# def c():
#     pass
#
#
# lst = [a, b, c]
# print(random.choice(lst))
# random.shuffle(lst)
# for func in lst:
#     print(func.__name__)

def some(x=0):
    print(x)
    if x < 1000:
        some(x + 1)

# [1] -> [2] -> [3] -> [4]
# [1] <- [2] <- [3] <- [4]

# some()


if __name__ == '__main__':
    print('Hey there!')
