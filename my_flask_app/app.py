import sqlite3
from flask import Flask, render_template, request, redirect

app = Flask(__name__)


@app.route('/')
def index():
    with sqlite3.connect('books.db') as connection:
        cursor = connection.cursor()
        cursor.execute("SELECT * FROM book")

        books = cursor.fetchall()
    return render_template('index.html', books=books)


@app.route('/add_book/', methods=['GET', 'POST'])
def add_book():
    if request.method == 'POST':
        with sqlite3.connect('books.db') as connection:
            query = f'INSERT INTO book (name, author, year) VALUES ("{request.form["name"]}", ' \
                    f'"{request.form["author"]}", "{request.form["year"]}")'
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect('/')

    return render_template('add_book.html')


@app.route('/delete_book/', methods=['GET', 'POST'])
def delete_book():
    if request.method == 'POST':
        with sqlite3.connect('books.db') as connection:
            cursor = connection.cursor()
            cursor.execute(f"DELETE FROM book WHERE id={request.form['book_id']}")
            connection.commit()
    return redirect('/')


@app.route('/edit_book/<int:book_id>/', methods=['GET', 'POST'])
def edit_book(book_id):
    with sqlite3.connect('books.db') as connection:
        cursor = connection.cursor()
        cursor.execute(f"SELECT * FROM book WHERE id={book_id}")

        book = cursor.fetchall()[0]

    if request.method == 'POST':
        with sqlite3.connect('books.db') as connection:
            query = f'UPDATE book SET name="{request.form["name"]}", author="{request.form["author"]}", ' \
                    f'year={request.form["year"]} WHERE id={book_id}'
            cursor = connection.cursor()
            cursor.execute(query)
            connection.commit()
            return redirect('/')
    return render_template('edit_book.html', book=book)


@app.route('/about/')
def about():
    return render_template('about.html')


if __name__ == '__main__':
    app.run(debug=True)
