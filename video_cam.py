import cv2

camera = cv2.VideoCapture("camera_detected_video1.mov")

if __name__ == '__main__':
    # rval, frame = camera.read()
    # cv2.imwrite('screenshot.jpg', frame)

    if camera.isOpened():
        rval, frame = camera.read()
    else:
        rval = False

    counter = 0
    while rval:
        cv2.imshow("preview", frame)
        rval, frame = camera.read()
        key = cv2.waitKey(20)
        if key == 27:  # exit on ESC
            break
