# ==========
# lst = []
# a = input()
# lst.append(a)  # ['user, value']
# sorted(lst[0])
# ==========
# a = [1, 2, 3]  # Math rule sort
# a = ['1', '2', '1']  # Alphabet rule sort

# while True:
#     user_value = input()
#     if user_value:
#         user_list = user_value.replace(' ,', ',').replace(', ', ',').split(',')
#     if user_value.isdigit():
#         user_value = int(user_value)
# a = {1, 3, 4}
# b = a.pop()
# print(b)  # 4
# print(a)  # {1, 3}

# try:
#     1 / 0
#     my_file = open('file_for_tests.txt', 'w')
#     my_file.write('Goodbye!')
#     my_file.close()
# except:
#     my_file.close()
# finally:
#     my_file.close()

# with open('file_for_tests.txt', 'r') as my_file:
#     text = my_file.read()
#     print(my_file.read())
#
#     with open('new.txt', 'w') as file2:
#         file2.write(my_file.read())
#
#     print('file2 written')
#
# print('Program end')
# print(my_file.readlines())

# with open('file_for_tests.txt', 'r') as my_file:
#     print('!', my_file.read(), '!')
#
#     # ....
#     print(my_file.tell())
#     my_file.seek(0)
#     print('!', my_file.read(), '!')

# =====

# with open('file_for_tests.txt', 'r') as my_file:
#     print('!', my_file.read(), '!')
#
#     my_file.seek(0)
#     print('!', my_file.read(), '!')

# a = []
# try:
#     print(a[0])
#     1 / 0
# except ZeroDivisionError as error:
#     print(error)
#     print('Zero division error')
# except IndexError:
#     print('Index error')
# except (KeyError, ValueError):
#     print('ERROR')
# except Exception as error:
#     print(dir(error))
# except:
#     pass
while True:
    try:
        user_input = input('Enter int: ')
        user_input = int(user_input)
    except ValueError:
        print('Are you stupid? Enter int')
    else:
        print('You are smart!')
    finally:
        print('Next try!')

# raise Exception("Hahaha error error")

