# def some(a: int) -> int:
#     return a * 10
#


# lst = ['1', '2', '3', '4']
#
# lst = "1234"


# neg_lst = [f"-{x}" for x in range(10)]
# pos_lst = [str(x) for x in range(10)]
#
# nums = neg_lst + pos_lst


# print(type(123))
# class Meta(type):
#
#     @classmethod
#     def __new__(cls, *args, **kwargs):
#         # cls.first_name = "Loki"
#         # cls.last_name = "Dess"
#         # print(args)
#         # print(kwargs)
#         return super(Meta, cls).__new__(*args, **kwargs)

#
# User = type("User", (), {})
#
# user = User()

# User = Meta("User", (), {
#     # '__str__': lambda self: f"{self.first_name} {self.last_name}"
# })
#
# user = User()
#
# print(user)
import random

import time


def my_decor(func):

    def wrapper(*args, **kwargs):
        return func(*args, **kwargs)
    return wrapper


class NoPrivacy(type):

    @classmethod
    def __new__(cls, *args, **kwargs):
        for attr in args[-1].keys():
            if args[1] in attr:
                raise Exception('No privacy for you!')
        return super(NoPrivacy, cls).__new__(*args, **kwargs)
    
    def __call__(cls, *args, **kwargs):
        return super(NoPrivacy, cls).__call__(*args, **kwargs)


@my_decor
class MySuperClass(metaclass=NoPrivacy):

    super_private_property = 10

    def __getattribute__(self, item):
        return super(MySuperClass, self).__getattribute__(item)

    def my_class_method(self):
        time.sleep(1)

    # def __delattr__(self, item):
    #     return super(MySuperClass, self).__delattr__(item)

    # def __init__(self):
    #     pass


my_instance = MySuperClass()
# my_instance.my_class_method()


class Cat:
    name = None
    gender = 'male'
    age = 10.0

    def __init__(self, name, age=10.0, gender='male'):
        self.name = name
        self.gender = gender
        self.age = age

    def __str__(self):
        if self.age < 1:
            return f"Kitty {self.name}"
        return f"Cat {self.name}"

    def __add__(self, other):
        if self.gender != other.gender:
            return Cat(
                name=self.name + other.name,
                gender=random.choice([self.gender, other.gender]),
                age=0.1
            )
        raise Exception('Same gender not allowed')

    def __radd__(self, other):
        return self.__add__(other)

    def __gt__(self, other):
        return self.age > other.age

    def __bool__(self):
        return False

    def __len__(self):
        return self.age

    def __iadd__(self, other):
        self.age += other
        return self

    def __enter__(self):
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        pass


class Dog:
    name = "Doggie"
    gender = 'female'


dog = Dog()

thor = Cat(name="Thor", age=11)
stragella = Cat(name="Stragella", gender="female")

cat_dog = dog + thor
thor += 1
print(thor.age)
# print(cat_dog)
# if thor > stragella:
#     little_kitty = thor + stragella
#     print(little_kitty)

# if len(thor):
#     print(123)


# with open():
#     pass

with Cat(name='Zuma') as cat:
    pass
