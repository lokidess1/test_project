# aiohttp, aiofiles, requests
import aiofiles
import aiohttp
import requests
import time
import asyncio


# def count_down():
#     for x in range(10):
#         time.sleep(1)
#         print(x)
#
#
# def count_up():
#     lst = list(range(10))
#     lst.reverse()
#
#     for x in lst:
#         time.sleep(1)
#         print(x)
#
#
# if __name__ == '__main__':
#     count_up()
#     count_down()

# async def count_down():
#     for x in range(10):
#         await asyncio.sleep(1)
#         print(x)
#
#
# async def count_up():
#     lst = list(range(10))
#     lst.reverse()
#
#     for x in lst:
#         await asyncio.sleep(1)
#         print(x)
#
#
# if __name__ == '__main__':
#     # count_up()
#     # count_down()
#     event_loop = asyncio.get_event_loop()
#     tasks = [
#         event_loop.create_task(count_up()),
#         event_loop.create_task(count_down())
#     ]
#     tasks = asyncio.wait(tasks)
#     event_loop.run_until_complete(tasks)

# image_count = input('Enter Image Count to Download: ')
#
# if image_count.isdigit():
#     t1 = time.time()
#     for counter in range(int(image_count)):
#         response = requests.get('https://loremflickr.com/320/240', allow_redirects=True)
#         with open(f'images/image_{counter}.jpg', 'wb') as image:
#             image.write(response.content)
#             print(f'Done with {counter}')
#     print(time.time() - t1)


async def download(session, page_number, per_page=10):
    page_start = (page_number + 1) * per_page
    for counter in range(per_page):
        async with session.get('https://loremflickr.com/320/240', allow_redirects=True, verify_ssl=False) as response:
            img_data = await response.read()
            async with aiofiles.open(f'images/image_{page_start + counter}.jpg', 'wb') as image_file:
                await image_file.write(img_data)


async def main(images_count):
    tasks = []
    async with aiohttp.ClientSession() as session:
        for counter in range(1, int((int(images_count) + 10) / 10)):
            tasks.append(
                asyncio.ensure_future(download(session, counter))
            )
        await asyncio.gather(*tasks)


if __name__ == '__main__':
    images_count = input('Enter Image Count to Download: ')
    t1 = time.time()
    if images_count.isdigit():
        event_loop = asyncio.get_event_loop()
        event_loop.run_until_complete(main(int(images_count)))
    print(time.time() - t1)


