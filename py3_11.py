# assert 0 > 1


# def my_method(var):
#     assert isinstance(var, int), "Must be int!"
#     # if isinstance(var, int):
#     #     raise Exception("Must be int!")


# my_method('10')
from unittest import TestCase


def my_method(value):
    return 20 if value > 10 else 50


# class BaseDemoTestCase(TestCase):
#
#     def test_my_method(self):
#         expected_value = 20
#         value = my_method(11)
#         self.assertEqual(expected_value, value)
#
#     def test_my_method_else(self):
#         expected_value = 50
#         value = my_method(1)
#         self.assertEqual(expected_value, value)
#
#
# class DemoTestCase(BaseDemoTestCase):
#     pass
#
#
# class DemoTestCase2(BaseDemoTestCase):
#     pass
#
#
# class DemoTestCase3(BaseDemoTestCase):
#     pass
# DO NOT DO LIKE THIS!

