from kivy.app import App
from kivy.properties import ObjectProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout


class MainLayout(FloatLayout):
    text_label = ObjectProperty(None)

    def __init__(self, **kwargs):
        super(MainLayout, self).__init__(**kwargs)
        # buttons_layout = BoxLayout()
        for x in range(5):
            self.ids.buttons.add_widget(
                Button(text=str(x))
            )
        # self.add_widget(buttons_layout)

    def button_press(self):
        self.ids.text_label.font_size += 5
        self.ids.label_layout.height += 10


class TextEditor(App):
    pass


if __name__ == '__main__':
    TextEditor().run()
