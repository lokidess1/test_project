# book = {
#     'name': '',
#     'year': 0,
#     'author': ''
# }


class Lib:

    books = [{
        'name': 'Loki',
    }, {
        'name': 'Thor',
    }, {
        'name': 'Tur'
    }]

    def delete(self, name_to_delete):
        self.books = list(filter(lambda x: x['name'] != name_to_delete, self.books))


if __name__ == '__main__':

    lib = Lib()
    lib.delete('Loki')
    print(lib.books)


