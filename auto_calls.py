from twilio.rest import Client


client = Client(
    'AC9264d8423d6230a440d1cc47fa18aae9',
    '83aaf85da9cea0882482c9f0ed02e04c',
)

TWILLIO_PHONE = '+12029315696'

CALL_TO_NUMBERS = ['+380992847330']

TWIML_INSTRUCTIONS_URL = "http://static.fullstackpython.com/phone-calls-python.xml"


for number in CALL_TO_NUMBERS:
    client.calls.create(
        to=number,
        from_=TWILLIO_PHONE,
        url=TWIML_INSTRUCTIONS_URL,
        method='GET'
    )
    print(f"Dial to {number}")
