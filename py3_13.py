class User:
    name = ''
    discounts = []

    def __init__(self, name, discounts=None):
        self.name = name
        self.discounts = discounts if discounts else []


class Price:

    def __get__(self, instance, owner):
        if instance.name in instance.user.discounts:
            return instance.tmp_price * 0.9 * instance.sale
        return instance.tmp_price

    def __set__(self, instance, value):
        instance.tmp_price = value


class Product:
    name = ""
    user = None
    price = Price()  # DESCRIPTOR SHOULD BE USED ONLY THIS WAY!!!!
    sale = 0.2

    def __init__(self, price, name, user):
        self.price = price
        self.name = name
        self.user = user


class Order:
    products = []

    def __init__(self, products):
        self.products = products

    def total(self):
        return round(sum([x.price for x in self.products]), 2)


if __name__ == '__main__':

    buyer = User(name='Loki', discounts=['Arrow'])

    sword = Product(price=10, name='Sword', user=buyer)
    arrow = Product(price=15, name='Arrow', user=buyer)

    order = Order(products=[sword, sword, sword, arrow, arrow])

    print(order.total())
