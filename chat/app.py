from datetime import datetime

from sqlalchemy.orm import sessionmaker
from tornado import web, websocket

from models import ChatRoom, Message

from db import engine


class Index(web.RequestHandler):

    def get(self):
        session = sessionmaker(bind=engine)()
        rooms = session.query(ChatRoom).all()
        self.render(
            'templates/index.html', rooms=rooms
        )


class AddRooms(web.RequestHandler):

    def get(self):
        self.render('templates/add_rooms.html', error=None)

    def post(self):
        session = sessionmaker(bind=engine)()
        name = self.get_body_argument('name')
        if name:
            session.add(
                ChatRoom(name=name)
            )
            session.commit()
            self.redirect('/')
        else:
            error = "You stupid! Enter the name of the chat!"
            self.render('templates/add_rooms.html', error=error)


class Chat(web.RequestHandler):

    def get(self, chat_id):
        session = sessionmaker(bind=engine)()
        messages = session.query(Message).filter(Message.chat_room == chat_id).all()
        self.render('templates/chat.html', messages=messages, chat_id=chat_id)


class SaveMessage(web.RequestHandler):

    def post(self):
        chat_id = self.get_body_argument('chat_id')
        session = sessionmaker(bind=engine)()
        session.add(
            Message(
                text=self.get_body_argument('text'),
                sender=self.get_body_argument('nickname'),
                chat_room=chat_id,
                created_at=datetime.now()
            )
        )
        session.commit()

        messages = session.query(Message).filter(Message.chat_room == chat_id).all()
        ChatUpdate.send_message(
            message=self.render_string('templates/chat_messages.html', messages=messages),
            chat_id=chat_id
        )


class ChatUpdate(websocket.WebSocketHandler):
    clients = []

    def open(self, *args: str, **kwargs: str):
        self.chat_id = args[0]
        ChatUpdate.clients.append(self)
        super(ChatUpdate, self).open(*args, **kwargs)

    def close(self, code=None, reason=None):
        ChatUpdate.clients.remove(self)

    @classmethod
    def send_message(cls, message, chat_id):
        for client in filter(lambda x: x.chat_id == chat_id, cls.clients):
            try:
                client.write_message(message)
            except websocket.WebSocketClosedError:
                pass
