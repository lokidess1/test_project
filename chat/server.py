from tornado import web, ioloop

from app import Index, AddRooms, Chat, SaveMessage, ChatUpdate

if __name__ == '__main__':

    app = web.Application([
        ('/', Index),
        ('/add_chat_room/', AddRooms),
        (r'/chat/(\d+)', Chat),
        ('/save_message/', SaveMessage),
        (r'/chat_update/(\d+)', ChatUpdate)
    ], debug=True)

    app.listen(8888)
    ioloop.IOLoop.current().start()
