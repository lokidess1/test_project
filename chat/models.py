from sqlalchemy import Column, Integer, String, ForeignKey, DateTime, Text
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship

Base = declarative_base()


class ChatRoom(Base):
    __tablename__ = 'chat_rooms'

    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String, nullable=False)
    messages = relationship('Message')


# class User(Base):
#     __tablename__ = 'users'
#
#     id = Column(Integer, primary_key=True, autoincrement=True)
#     nickname = Column(String, nullable=False)
#     sender_from = relationship('Message')


class Message(Base):
    __tablename__ = 'messages'

    id = Column(Integer, primary_key=True, autoincrement=True)
    sender = Column(String)
    chat_room = Column(Integer, ForeignKey('chat_rooms.id'))
    created_at = Column(DateTime)
    text = Column(Text)
