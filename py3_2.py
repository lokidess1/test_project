lst = [1, 2, 3, 'asd', 3.5, [1, 2, 3]]
array = [1, 2, 3, 4]
lst1 = list('hello')
# print(lst1)
# lst[2] = 123
# print(lst[2])
# array_len = len(array)
# print(array[array_len - 1])
# print(array[-4])

# a = [1, 2, 3]
# b = a.copy()
# a[0] = 'hello'
# print(b)

new_array = [
    lst[1],
    lst[2],
    lst[3],
    lst[4],
]
lst.insert(0, 'hello')
# print(lst)
# print(lst[::-1])  # lst.reverse()

# print(list(range(51, 100, 2)))
# lst = [0, None, False, '', []]
# print(sum(lst))
# print(min(lst))
# print(max(lst))

# print(all(lst))
# print(any(lst))

# lst.remove(2)
# print(lst)
# lst.sort()
# print(lst)
# lst1 = ['qwe', 'asd', 'vdsree', 'fv']
#
# lst1.sort(key=len)
# print(lst1)

# matrix = [
#     [1, 2, 3],
#     [4, 5, 6],
#     [7, 8, 9]
# ]
#
# print(matrix[1][1])
# a = [1, 2, 3]
# tpl = (1, 2, 3, a.copy())
# a.append(4)
# tpl[0] = 123
# tpl[-1][0] = 123
# tpl[-1].append(4)
# print(tpl)
# lst = [1, 2, 3, 2, 4, 2]
# lst = list(set(lst))
# print(lst)
# my_set = {1, 2, 1, (1, 2, 3)}
# print(my_set)

# text = 'asd qwe qwe asd'
# print(text.rfind('d'))

# text.replace('a', 'A')
# print(text.count(' ') + 1)
#
# lst = ['1', '2', '3', '4']
#
# print('asdasdasd'.join(lst))
# print(text.center(30, '*'))

text = "____________qweqweqwe_____________"
print(text.strip('_'))

# greet = "Hey! {name} {age}".format(age=30, name='Loki')
#
# print(greet)

name = 'Loki'
age = 30
text = f"Hey! {name.upper()} your age {age + 1}"
print(text)
print(name)
