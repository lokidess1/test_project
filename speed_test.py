import random


# def outliner(integers):
#     odd = []
#     even = []
#
#     for number in integers:
#         if number % 2 == 0:
#             even.append(number)
#         elif number % 2 == 1:
#             odd.append(number)
#
#     if len(odd) == 1:
#         return odd[0]
#
#     elif len(even) == 1:
#         return even[0]


# def outliner(int):
#     odds = [x for x in int if x % 2 != 0]
#     evens = [x for x in int if x % 2== 0]
#     return odds[0] if len(odds) < len(evens) else evens[0]


def outliner(lst):
    lst_set = set(lst)
    even = set([x for x in lst if x % 2 == 0])
    intersection = lst_set.intersection(even)
    return lst_set.difference(even).pop() if len(intersection) > 1 else intersection.pop()


count = 1000000000
data_odd = list(range(1, count, 2))
data_odd.insert(random.randrange(0, count), 2)

data_even = list(range(2, count, 2))
data_even.insert(random.randrange(0, count), 1)
print(outliner(data_odd))
print(outliner(data_even))
