import time
from random import randrange

length = 200000000
sequince = list(range(1, length, 2))
pop_index = randrange(1, length / 2 - 1)
print(sequince.pop(pop_index))


def timeit(method):
    def timed(*args, **kw):
        ts = time.time()
        result = method(*args, **kw)
        te = time.time()
        if 'log_time' in kw:
            name = kw.get('log_name', method.__name__.upper())
            kw['log_time'][name] = int((te - ts) * 1000)
        else:
            print('%r  %2.2f s' % (method.__name__, (te - ts)))
        return result
    return timed

@timeit
def find_missing(inner_seq):
    step = inner_seq[1] - inner_seq[0]
    full_seq = set(range(1, inner_seq[-1] + 1, step))
    return full_seq.difference(set(inner_seq))

# @timeit
# def find_missing(seq):
#     return int((seq[-1] + seq[0]) * (len(seq) + 1) // 2 - sum(seq))

# @timeit
# def find_missing(seq):
#     step = int(seq[1] - seq[0])
#     multB = set(range(min(seq), max(seq), step))
#     return multB.difference(set(seq))

@timeit
def lost_value(lst):
    start_index = 0
    end_index = len(lst) - 1
    step = (lst[end_index] - lst[start_index]) // len(lst)
    while end_index - start_index > 1:
        center_index = (end_index + start_index) // 2
        if lst[center_index] - lst[start_index] == (center_index - start_index) * step:  # ok
            start_index = center_index
        else:
            end_index = center_index
    return lst[start_index] + step


print(find_missing(sequince))
