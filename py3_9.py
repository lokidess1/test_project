class Animal:
    pass


class FlightAnimal(Animal):

    def __init__(self, name):
        pass

    def move(self):
        pass


class WalkAnimal(Animal):

    def move(self):
        pass


class Bird(WalkAnimal, FlightAnimal, Animal):
    pass
# def __init__(self, *args, **kwargs):
#     WalkAnimal.__init__(self, kwargs['name'])
#     FlightAnimal.__init__(self, kwargs['speed'])


# print(Bird.__mro__)
# bird = Bird('asd')


class MyClass:
    name = 'Loki'
    coins = 102

    @staticmethod
    def some_cool_method():
        print("Cool stuff happens!")

    @classmethod
    def my_class_method(cls):
        # cls.some_cool_method()
        # print(f'Class method here {cls.name}')
        cls.name = 'Thor'

    def show_your_self(self):
        print(f"{self.name}")
        self.some_cool_method()

    @property
    def money(self):
        value = str(self.coins / 10).split('.')
        return {'gold': int(value[0]), 'silver': int(value[1])}

    @money.setter
    def money(self, value):
        self.coins += value['silver']
        self.coins += value['gold'] * 10

    @money.deleter
    def money(self):
        self.coins = 0


# MyClass.my_class_method()
# loki = MyClass()
# print(loki.name)
# MyClass().my_class_method()
# MyClass.some_cool_method()
# MyClass().some_cool_method()

loki = MyClass()
print(loki.money)
# loki.add_money({'gold': 7, 'silver': 2})
# loki.money = {'gold': 7, 'silver': 2}
# del loki.money
# print(loki.money)


# isinstance()
bird = Bird('asdasd')
# ...
# ...
# bird = Animal()

# if bird.__class__ == Bird.__class__:
# if isinstance(bird, Bird):
#     print('Still a Bird')

# NOT WORKING
# value = input()
# if isinstance(value, int):
#     pass


# hasattr()
# bird.name = 'Flora'
#
# if hasattr(bird, 'move'):
#     print(bird.name)


# getattr()
# while True:
#     value = input('What you wanna see? ')
#
#     if value == 'exit':
#         break
#
#     print(getattr(loki, value, "Loki don't have it"))

# setattr()

# loki.bag = ['item1', 'item2']
# setattr(loki, 'bag1', ['3', '4'])
# print(loki.bag1)


# delattr()
# delattr(loki, 'coins')
# print(loki.coins)
